using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ShellManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Rigidbody2D rbody;
    [SerializeField] CircleCollider2D triggerCollider;
    [SerializeField] CircleCollider2D terrainCollider;
    [SerializeField] ParticleSystem deathPSPrefab;
    [SerializeField] ParticleSystem firePSPrefab;
    [SerializeField] ParticleSystem smokePSPrefab;
    [SerializeField] ParticleSystem flamePSPrefab;

    public Rigidbody2D RBody => rbody;

    PlayerManager playerManager;
    BotManager botManager;
    WeaponMeta weaponMeta;
    Coroutine damageCO;
    Tilemap tilemap;
    float impactDamage;
    int destroyTime = 50;
    bool isAlly; // true - ������ ������
    bool isDead; // true - ������ "����", �.�. ����� �������� ������ ������������� ����

    // �������������� ������ ������
    public void Init(WeaponMeta _weaponMeta)
    {
        weaponMeta = _weaponMeta;

        Init(true, weaponMeta.ImpactDamage);
    }

    // �������������� ������
    public void Init(bool _isAlly, float damage)
    {
        isAlly = _isAlly;
        impactDamage = damage;

        CreateSmokeParticles();
        Destroy(gameObject, destroyTime);
    }

    // ������� ��� ������
    void CreateDeathParticles()
    {
        var deathPS = Instantiate(deathPSPrefab);
        deathPS.transform.position = transform.position;
        Destroy(deathPS.gameObject, 60);
    }

    // ������� ���� ��� ��������
    void CreateSmokeParticles()
    {
        var smokePS = Instantiate(smokePSPrefab);
        smokePS.transform.position = transform.position;
        Destroy(smokePS, 5);
    }

    // ������� ����
    void CreateFlameParticles()
    {
        var flamePS = Instantiate(flamePSPrefab);
        flamePS.transform.position = transform.position;
        Destroy(flamePS, 5);
    }

    // ��������� �������
    void TerrainDestructor(Collision2D collision)
    {
        if (tilemap != null)
        {
            Vector3 hitPosition = Vector3.zero;

            foreach (ContactPoint2D hit in collision.contacts)
            {
                hitPosition.x = hit.point.x - 0.01f * hit.normal.x;
                hitPosition.y = hit.point.y - 0.01f * hit.normal.y;
                tilemap.SetTile(tilemap.WorldToCell(hitPosition), null);
            }
        }
    }

    // "�������" ������
    void Die()
    {
        if (!isDead) terrainCollider.enabled = true;

        isDead = true;
        triggerCollider.enabled = false;
        transform.GetChild(0).gameObject.SetActive(false);

        CreateDeathParticles();
        CreateFlameParticles();
        Destroy(terrainCollider.gameObject, 1);

        SoundManager.Instance.Play(SoundName.Hit, true);
        CameraController.Instance.Shake();
    }

    // �������� �������� ������������� ����
    void StartPeriodicDamage()
    {
        damageCO = StartCoroutine(PeriodicDamage());

        var firePS = Instantiate(firePSPrefab, botManager.transform);
        firePS.transform.position = botManager.transform.position;

        Destroy(firePS.gameObject, weaponMeta.FireDuration);
        StartCoroutine(PeriodicDamageTimer());
    }

    // ������� ������������� ����
    IEnumerator PeriodicDamage()
    {
        while (botManager != null)
        {
            botManager.DoDamage(weaponMeta.PeriodicDamage, false);

            yield return new WaitForSeconds(weaponMeta.DamageInterval);
        }
    }

    // ������ �������������� �����
    IEnumerator PeriodicDamageTimer()
    {
        yield return new WaitForSeconds(weaponMeta.FireDuration);

        if (damageCO != null)
            StopCoroutine(damageCO);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isDead)
        {
            if (isAlly)
            {
                botManager = collision.transform.GetComponent<BotManager>();

                if (botManager != null)
                {
                    botManager.DoDamage(impactDamage);

                    // ���� ������ ������������ ����, �������� �������� ������������� ����
                    if (weaponMeta.WeaponType == WeaponType.Fiery) StartPeriodicDamage();
                    // ���� ������ ��������������� ����, ������������� ��� ����� �� n �������
                    else if (weaponMeta.WeaponType == WeaponType.Frozen) botManager.FreezeState(weaponMeta.SkipTurnsCount);
                }
            }
            else
            {
                playerManager = collision.transform.GetComponent<PlayerManager>();

                if (playerManager != null) playerManager.DoDamage(impactDamage);
            }
        }
        else
        {
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            GameManager.Instance.OnMoveEnded();
        }

        tilemap = collision.transform.GetComponent<Tilemap>();

        Die();

        if (collision.transform.CompareTag("Destroyable")) TerrainDestructor(collision);
    }
}
