using UnityEngine;

public class Aim : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Camera cam;
    [SerializeField] Transform crosshair;
    [SerializeField] Transform spawnPoint;
    [SerializeField] Transform weaponPivot;
    [SerializeField] Transform pointsContainer;
    [SerializeField] GameObject trajectoryPoint;

    // Other
    public Transform SpawnPoint => spawnPoint;
    public Vector3 CursorPosition => cursorPosition;
    public Vector3 LookDirection => lookDirection;
    public float LaunchSpeed => playerSettings.LaunchSpeed;
    public float ShellGravityScale => playerSettings.ShellGravityScale;
    public int MoveDirection { get; set; }
    public bool IsAiming { get; set; }

    GameObject[] points;
    PlayerSettings playerSettings;
    PlayerManager playerManager;
    Vector3 cursorPosition;
    Vector3 lookDirection;
    Vector3 gunDirection;
    float lookAngle;
    bool showTrajectory;
    bool isAndroid;

    void Start()
    {
        playerSettings = Director.Instance.PlayerSettings;
        showTrajectory = !Director.Instance.GameMeta.DefaultCursor;
        playerManager = GetComponent<PlayerManager>();

#if UNITY_ANDROID
        isAndroid = true;
#endif

        if (!showTrajectory) return;

        points = new GameObject[playerSettings.PointsCount];

        for (int i = 0; i < playerSettings.PointsCount; i++)
        {
            points[i] = Instantiate(trajectoryPoint, transform.position, Quaternion.identity, pointsContainer);
            points[i].GetComponent<SpriteRenderer>().color = playerSettings.TrajectoryColor;
        }

        PointsState(false);
    }

    void Update()
    {
        //GetCursorPosition();
        MoveCrosshair();
    }

    // ������� �������
    void GetCursorPosition()
    {
        Vector2 pos = isAndroid ? playerManager.TouchPosition : (Vector2)Input.mousePosition;

        cursorPosition = cam.ScreenToWorldPoint(pos);
        cursorPosition.z = 0;

        lookDirection = transform.position - cursorPosition;
        gunDirection = cursorPosition - transform.position;
    }
    
    // ������� ������
    void GunRotation()
    {
        if (IsAiming)
            lookAngle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg;
        else
            lookAngle = Mathf.Atan2(gunDirection.y, gunDirection.x) * Mathf.Rad2Deg ;

        weaponPivot.rotation = Quaternion.AngleAxis(lookAngle, Vector3.forward);
    }

    // �������������� ����
    void FlipGun()
    {
        if (crosshair.localPosition.x > 0.1f)
        {
            weaponPivot.localScale = IsAiming ? Vector2.one : new Vector2(1, -1);
        }
        else if (crosshair.localPosition.x < -0.1f)
        {
            weaponPivot.localScale = IsAiming ? new Vector2(1, -1) : Vector2.one;
        }
    }

    // ������� ������
    void MoveCrosshair()
    {
        crosshair.localPosition = Vector2.ClampMagnitude(lookDirection.normalized * 10, playerSettings.AimDistance);

        GunRotation();
        FlipGun();
    }

    public void CheckMoveDirection()
    {
        GetCursorPosition();

        if (Mathf.Abs(lookDirection.x) > 0.1f)
            MoveDirection = cursorPosition.x > transform.position.x ? 1 : -1;
        else
            MoveDirection = 0;
    }

    // ��������� ���������� �������
    public void UpdateTrajectory()
    {
        if (!showTrajectory) return;

        Vector2 lookDirection = (transform.position - cursorPosition).normalized;

        for (int i = 0; i < playerSettings.PointsCount; i++)
            points[i].transform.position = PointPosition(lookDirection, i * playerSettings.TrajectoryTimeMultilpier);
    }

    // ��������/��������� ����� � ����������
    public void PointsState(bool show)
    {
        if (show && !showTrajectory) return;

        foreach (var point in points)
            point.SetActive(show);
    }

    // ������� ������� ��� ����� � ����������
    Vector2 PointPosition(Vector2 lookDirection, float t)
    {
        Vector2 currentPosition = (Vector2)spawnPoint.position + (lookDirection * playerSettings.LaunchSpeed * t) +
            0.5f * (Physics2D.gravity * playerSettings.ShellGravityScale) * (t * t);

        return currentPosition;
    }
}
