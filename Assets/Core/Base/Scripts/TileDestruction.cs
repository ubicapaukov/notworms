using UnityEngine;
using UnityEngine.Tilemaps;

public class TileDestruction : MonoBehaviour
{
    Transform player;
    Tilemap tilemap;

    void Start()
    {
        tilemap = GetComponent<Tilemap>();
        player = GameManager.Instance.PlayerManager.transform;
    }

    public void DestroyTileAt(ContactPoint2D hit)
    {
        Vector3 hitPosition = Vector3.zero;
        hitPosition.x = hit.point.x - 0.01f * hit.normal.x;
        hitPosition.y = hit.point.y - 0.01f * hit.normal.y;

        tilemap.SetTile(tilemap.WorldToCell(hitPosition), null);
    }
    /*
    void OnCollisionEnter2D(Collision2D collision)
    {
        Vector3 hitPosition = Vector3.zero;
        Debug.Log("Contacts " + collision.contactCount);

        foreach (ContactPoint2D hit in collision.contacts)
        {
            hitPosition.x = hit.point.x - 0.01f * hit.normal.x;
            hitPosition.y = hit.point.y - 0.01f * hit.normal.y;
            tilemap.SetTile(tilemap.WorldToCell(hitPosition), null);
        }
    }*/
}
