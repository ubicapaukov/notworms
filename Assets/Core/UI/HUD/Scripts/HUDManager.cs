using TMPro;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] float animationTime;
    [SerializeField] float scaleMultiplier;
    [SerializeField] float hitFlashFadeTime;
    [SerializeField] float aimTipFadeTime;
    [SerializeField] Color defaultWeaponSlotColor;
    [SerializeField] Color playerTurnColor;
    [SerializeField] Color enemyTurnColor;
    [SerializeField] Color aimTipFillColor;
    [SerializeField] Gradient aimTipFillGradient;

    [Header("References")]
    [SerializeField] TextMeshProUGUI turnSideTMP;
    [SerializeField] TextMeshProUGUI turnTMP;
    [SerializeField] TextMeshProUGUI timerTitleTMP;
    [SerializeField] TextMeshProUGUI timerTMP;
    [SerializeField] Image aimTipFill;
    [SerializeField] Image hitFlash;
    [SerializeField] CanvasGroup aimTipCG;
    [SerializeField] CanvasGroup timerCG;
    [SerializeField] WeaponSlot[] weaponSlots;

    WeaponSlot currentWeapon;
    Sequence gradient;
    Tweener fillTwn;
    Tweener fadeTwn;
    Tweener timerTwn;

    public void Init()
    {
        currentWeapon = weaponSlots[0];
        weaponSlots[0].Select(true, scaleMultiplier, 0, defaultWeaponSlotColor);
    }

    // �������� ������
    public void SelectWeapon(int index)
    {
        currentWeapon.Select(false, scaleMultiplier, animationTime, defaultWeaponSlotColor);

        currentWeapon = weaponSlots[index];

        currentWeapon.Select(true, scaleMultiplier, animationTime, defaultWeaponSlotColor);
    }

    // ��������� ����� � ����������� � ������� �������
    public void UpdateSideTurnText(bool show, bool isPlayerTurn = false)
    {
        string side = isPlayerTurn ? "Player's" : "Enemy's";

        turnTMP.text = show ? "turn" : "";
        turnSideTMP.text = show ? side : "";
        turnSideTMP.color = isPlayerTurn ? playerTurnColor : enemyTurnColor;
    }

    // ��������� ����� � ����������� � ���������� � ����
    public void UpdatePreparationText(float time, bool show = true)
    {
        timerTitleTMP.text = show ? "preparation" : "move ends in";
        timerTMP.text = time.ToString("F0");
    }

    // ��������� ����� �������
    public void UpdateTimer(float time)
    {
        timerTMP.text = time.ToString("F0");
    }

    // ��������/��������� ������ ������ ������
    public void WeaponSlotButtonsState(bool activate)
    {
        foreach (var slot in weaponSlots)
            slot.Button.interactable = activate;
    }

    // "�������" ��� ���������
    public void TriggerHitFlash()
    {
        hitFlash.DOFade(0.6f, hitFlashFadeTime).OnComplete(() =>
        {
            hitFlash.DOFade(0, hitFlashFadeTime);
        });
    }

    // ��������/��������� ��������� ������������
    public void AimTipState(bool show, float duration = 0)
    {
        gradient.Complete();
        fillTwn.Complete();
        fadeTwn.Complete();
        timerTwn.Complete();

        if (show)
        {
            aimTipFill.color = aimTipFillColor;
            gradient = aimTipFill.DOGradientColor(aimTipFillGradient, duration);
        }
        else aimTipFill.color = Color.black;

        fillTwn = aimTipFill.DOFillAmount(show ? 1 : 0, duration);
        fadeTwn = aimTipCG.DOFade(show ? 1 : 0, show ? aimTipFadeTime : 0);
        timerTwn = timerCG.DOFade(show ? 0 : 1, aimTipFadeTime);
    }
}
