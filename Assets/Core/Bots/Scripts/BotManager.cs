using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BotManager : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] float raycastDistance;
    [SerializeField] float jumpStrength;
    [SerializeField] float jumpCD;
    [SerializeField] float stuckTime;

    [SerializeField] float checkRadius;
    [SerializeField] LayerMask groundLayer;

    [Header("References")]
    [SerializeField] TextMeshProUGUI nicknameTMP;
    [SerializeField] Image healthSlider;
    [SerializeField] Transform aimPoint;
    [SerializeField] Transform groundCheck;
    [SerializeField] Transform movementPSPoint;
    [SerializeField] Rigidbody2D rbody;
    [SerializeField] ShootPoint shootPointPrefab;
    [SerializeField] ShellManager shellPrefab;
    [SerializeField] GameObject frozenEffect;
    [SerializeField] Animator eyesAnimator;
    [SerializeField] ParticleSystem deathPSPrefab;
    [SerializeField] ParticleSystem hitPSPrefab;
    [SerializeField] ParticleSystem walkPSPrefab;
    [SerializeField] ParticleSystem jumpPSPrefab;

    public int TurnsSkip { get; private set; }
    public bool MoveWasMade { get; set; }

    PlayerManager playerManager;
    BotsSettings botsSettings;
    GameManager gameManager;
    Transform shootPoint;
    Transform player;
    Transform bodyPivot;
    Animator animator;
    float currentStuckTime;
    float health;
    bool isJumpCheckOnCD;
    bool isWalkPSOnCD;
    bool hasShooted;
    bool onGround;
    bool canMove;
    bool canJump;
    bool landed;

    void Start()
    {
        gameManager = GameManager.Instance;
        botsSettings = Director.Instance.BotsSettings;
        playerManager = gameManager.PlayerManager;
        player = playerManager.transform;
        health = botsSettings.StartHealth;
        bodyPivot = transform.GetChild(0);
        animator = GetComponent<Animator>();

        SetNickname();
        gameManager.RegisterBot(this);
        StartCoroutine(Blinking());

        //Vector2 newPos = transform.position;
        //newPos.x = Random.Range(-48, 48);
        //transform.position = newPos;
    }

    void Update()
    {
        if (!canMove) return;

        onGround = Physics2D.OverlapCircle(groundCheck.position, checkRadius, groundLayer);

        StuckHandler();
        CheckWall();
        Jump();
        FlipBody();

        if (!landed && onGround)
        {
            landed = true;
            animator.SetBool("jump", false);
        }

        //animator.SetBool("isMoving", animPlay && onGround);
        //animator.SetBool("jump", !onGround);

        if (!isWalkPSOnCD)
        {
            CreateWalkParticles();
            StartCoroutine(WalkParticlesCreator());
        }
    }

    void FixedUpdate()
    {
        if (!canMove) return;

        Movement();
    }

    // ��������� ���� �� ����� �� ����
    void CheckWall()
    {
        for (int i = 0; i < 2; i++)
        {
            var hitsInfo = Physics2D.RaycastAll(transform.position, i == 0 ? Vector2.left : Vector2.right, raycastDistance, groundLayer);

            Debug.DrawRay(transform.position, (i == 0 ? Vector2.left : Vector2.right) * raycastDistance, Color.blue);

            if (hitsInfo.Length > 0)
            {
                canJump = true;
                currentStuckTime += Time.deltaTime;

                break;
            }
            else
            {
                canJump = false;
                currentStuckTime = 0;
            }
        }
    }

    void Jump()
    {
        if (canJump && !isJumpCheckOnCD)
        {
            landed = false;
            CreateJumpParticles();
            animator.SetBool("jump", true);
            rbody.AddForce(Vector2.up * jumpStrength, ForceMode2D.Impulse);

            StartCoroutine(JumpCD());
        }
    }
    
    void StuckHandler()
    {
        if (shootPoint == null) return;

        if (currentStuckTime > stuckTime)
        {
            PrepareShooting();
        }
    }

    void Movement()
    {
        Vector2 newPos = new Vector2(shootPoint.position.x > transform.position.x ? 1 : -1, 0);
        rbody.position += newPos * botsSettings.MoveSpeed * Time.fixedDeltaTime;
    }

    void Shoot()
    {
        // accuracy = 3.3f - 100% hit accuracy
        float height = Random.Range(botsSettings.MinShootHeight, botsSettings.MaxShootHeight);

        if (aimPoint.position.y >= height)
        {
            gameManager.OnMoveEnded();
            return;
        }

        float accuracy = Random.Range(botsSettings.MinAccuracy, botsSettings.MaxAccuracy);
        bool canShoot = BotAim.solve_ballistic_arc_lateral(aimPoint.position, player.position, accuracy, height, out Vector3 s0, out float s1);

        if (!canShoot) return;

        var shell = Instantiate(shellPrefab);
        shell.transform.position = aimPoint.position;
        shell.Init(false, botsSettings.Damage);
        shell.RBody.gravityScale = s1;
        shell.RBody.AddForce(s0 * accuracy, ForceMode2D.Impulse);

        SoundManager.Instance.Play(SoundName.Shoot, true);
    }

    // ������� ���� ����
    public void DoDamage(float amount, bool hitFlash = true)
    {
        float finalHealth = health - amount;

        if (finalHealth <= 0)
        {
            finalHealth = 0;

            Die();
        }
        else if (hitFlash)
        {
            var hitPs = Instantiate(hitPSPrefab);
            Vector2 newPos = transform.position;
            newPos.y += 1.2f;
            hitPs.transform.position = newPos;
            Destroy(hitPs.gameObject, 20);
        }

        health = finalHealth;
        healthSlider.fillAmount = health / botsSettings.StartHealth;

        if (hitFlash) gameManager.HUDManager.TriggerHitFlash();
    }

    // ����� �� ����� ������
    public void TurnState(bool _canMove)
    {
        canMove = _canMove;
        currentStuckTime = 0;
        animator.SetBool("isMoving", canMove);

        if (canMove)
        {
            if (shootPoint == null || Vector2.Distance(transform.position, player.position) > botsSettings.ShootPointDestroyDistance)
            {
                hasShooted = false;

                if (shootPoint != null)
                    Destroy(shootPoint.gameObject);

                float shootDistance = Random.Range(botsSettings.MinShootDistance, botsSettings.MaxShootDistance);
                var point = Instantiate(shootPointPrefab);

                point.SetPosition(player, transform.position.y, shootDistance);
                shootPoint = point.transform;
            }
        }
        else
        {
            animator.SetBool("jump", false);

            if (shootPoint != null) Destroy(shootPoint.gameObject);

            shootPoint = null;
        }
    }

    // "������������" �� ��������� �����
    public void FreezeState(int moves)
    {
        TurnsSkip += moves;
        frozenEffect.SetActive(TurnsSkip > 0);
    }

    // ������������� ��������� ���
    void SetNickname()
    {
        string[] names = Director.Instance.GameSettings.BotNames;
        string fName = names[Random.Range(0, names.Length)];

        nicknameTMP.text = "Bot " + fName;
        name = "Bot " + fName;
    }

    // �������������� ����
    void FlipBody()
    {
        if (shootPoint != null)
        {
            if (shootPoint.position.x > transform.position.x)
                bodyPivot.localScale = new Vector2(-1, 1);
            else if (shootPoint.position.x < transform.position.x)
                bodyPivot.localScale = Vector2.one;
        }
        else if (!canMove)
        {
            if (player.position.x > transform.position.x)
                bodyPivot.localScale = new Vector2(-1, 1);
            else if (player.position.x < transform.position.x)
                bodyPivot.localScale = Vector2.one;
        }
    }

    // ������� ����
    void Die()
    {
        var deathPS = Instantiate(deathPSPrefab);
        deathPS.transform.position = transform.position;
        Destroy(deathPS.gameObject, 60);

        gameManager.OnMoveEnded();
        gameManager.DeleteBot(this);
        Destroy(gameObject);
    }

    // ������ ������� ��� ������
    void CreateWalkParticles()
    {
        if (!onGround) return;

        var walkPS = Instantiate(walkPSPrefab);
        walkPS.transform.position = movementPSPoint.position;

        Destroy(walkPS.gameObject, 5);
    }

    void CreateJumpParticles()
    {
        var jumpPS = Instantiate(jumpPSPrefab);
        jumpPS.transform.position = movementPSPoint.position;
        Destroy(jumpPS.gameObject, 5);
    }

    void PrepareShooting()
    {
        if (shootPoint != null) Destroy(shootPoint.gameObject);

        shootPoint = null;
        hasShooted = true;
        canMove = false;
        FlipBody();
        gameManager.MovesSystem.StopTimer();
        StartCoroutine(ShootingDelay());
        animator.SetBool("isMoving", false);
    }

    // �������� ����� ���������
    IEnumerator ShootingDelay()
    {
        animator.SetBool("jump", false);

        yield return new WaitForSeconds(botsSettings.ShootDelay);

        Shoot();
    }

    // �������� ��������
    IEnumerator Blinking()
    {
        while (true)
        {
            float time = Random.Range(3f, 7f);

            yield return new WaitForSeconds(time);

            eyesAnimator.SetTrigger("blink");
        }
    }

    // ������ ������� ������
    IEnumerator WalkParticlesCreator()
    {
        isWalkPSOnCD = true;

        yield return new WaitForSeconds(0.5f);

        isWalkPSOnCD = false;
    }

    IEnumerator JumpCD()
    {
        isJumpCheckOnCD = true;

        yield return new WaitForSeconds(jumpCD);

        isJumpCheckOnCD = false;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (hasShooted || TurnsSkip > 0) return;

        if (collision.CompareTag("point"))
            if (collision.transform == shootPoint)
                PrepareShooting();
    }
}
